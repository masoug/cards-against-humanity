package netgame.cardsagainsthumanity;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class ClientGUI extends JFrame implements ActionListener {
	
	private Client parent;
	
	private JPanel centerDisplay;
	private JPanel westPanel;
	private JPanel northPanel;
	private JTextArea instructionArea;
	private JButton playCardButton;
	private CardPanel blackCardPanel;
	private JLabel pointsLabel;
	private JLabel timeLabel;
	private double timeRemaining;
	private javax.swing.Timer countdownTimer;

	// cards...
	private ArrayList<CardPanel> activeCards;
	
	// button enums...
	private final String PLAY_CARD = "Play card(s)";

	public ClientGUI(Client parent) {
		super("Cards Against Humanity - Client");
		
		this.parent = parent;

		// Initialize the GUI elements
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout(3, 3));

		JPanel southPanel = new JPanel();
		southPanel.setLayout(new FlowLayout());
		playCardButton = new JButton(PLAY_CARD);
		playCardButton.addActionListener(this);
		southPanel.add(playCardButton);
		mainPanel.add(southPanel, BorderLayout.SOUTH);

		instructionArea = new JTextArea("Loading...\n\nWaiting for more players to join...");
		instructionArea.setLineWrap(true);
		instructionArea.setWrapStyleWord(true);
		instructionArea.setEditable(false);

		westPanel = new JPanel();
		westPanel.setLayout(new BoxLayout(westPanel, BoxLayout.Y_AXIS));
		blackCardPanel = new CardPanel(new Card(CardType.BLK_REG, 0));
		westPanel.add(blackCardPanel);
		westPanel.add(instructionArea);
		mainPanel.add(westPanel, BorderLayout.WEST);

		northPanel = new JPanel();
		northPanel.setLayout(new FlowLayout());
		timeLabel = new JLabel("Time Remaining: ", JLabel.LEFT);
		timeLabel.setForeground(Color.RED);
		northPanel.add(timeLabel);
		pointsLabel = new JLabel("POINTS: -", JLabel.LEFT);
		pointsLabel.setForeground(Color.RED);
		northPanel.add(pointsLabel);
		mainPanel.add(northPanel, BorderLayout.NORTH);
		
		JScrollPane centerPanel = new JScrollPane();
		centerDisplay = new JPanel();
		centerDisplay.setLayout(new GridLayout(0, 5, 3, 10));
		centerPanel.getViewport().add(centerDisplay);
		mainPanel.add(centerPanel, BorderLayout.CENTER);
		
		activeCards = new ArrayList<CardPanel>();
		
		setContentPane(mainPanel);
		setSize(1250, 700);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		countdownTimer = new javax.swing.Timer(40, this);
		timeRemaining = 0.0;
	}

	public void startTimer(double duration) {
		timeRemaining = duration;
		countdownTimer.start();
	}

	public void stopTimer() {
		countdownTimer.stop();
	}
	
	public void setPoints(int points) {
		pointsLabel.setText("POINTS: "+points);
	}
	
	public void applyGameState(GameState state, int playerID) {
		// apply game state.
		System.out.println("Applying game state!");
		if (state.getMainPlayer() == null) // game hasn't started! wait.
			return;
		if (state.getMainPlayer().getID() == playerID) { // if we are the main player.
			System.out.println("Main Player.");
			if (state.numResponses() == state.numPlayers()-1) { // if all the players have responded
				System.out.println("All players have responded.");
				setInstructions("All the players have responded. \n\nPlease select a card to play.");
				addActiveCard(state.getLastResponse());
				updateActiveCards();
				enablePlayCards();
				startTimer(30f);
			} else { // if we are still waiting for players to respond
				System.out.println("Still waiting for players to respond. Adding active card.");
				System.out.println(""+state.numResponses()+" responses received.");
				setBlackCard(state.getBlackCard());
				if (state.numResponses() != 0)
					addActiveCard(state.getLastResponse());
				updateActiveCards();
				setInstructions(
					"You are the main player.\n\nPlease wait for all players to respond. "+
					"Once all players have responded, select one card that is best paired with the black card and play it."
				);
				disablePlayCards();
				stopTimer();
			}
		} else { // if we are just a normal player.
			System.out.println("Normal Player.");
			Player player = state.getPlayerFromID(playerID);
			setBlackCard(state.getBlackCard());
			if (activeCards.size() == 0)
				for (int i = 0; i < 10; i++)
					addActiveCard(player.getWhiteCard(i));
			updateActiveCards();
			setInstructions("Now select one card and push the \"Play Card(s)\" button.");
			//enablePlayCards();
		}
		setPoints(state.getPlayerFromID(playerID).getScore());
	}

	public void setInstructions(String instructions) {
		instructionArea.setText("INSTRUCTIONS:\n"+instructions);
	}

	public void enablePlayCards() {
		playCardButton.setEnabled(true);
	}

	public void disablePlayCards() {
		playCardButton.setEnabled(false);
	}
	
	public void setBlackCard(Card c) {
		if (!c.equals(blackCardPanel.getCard())) { // is the card is not different.
			blackCardPanel.setCard(c);
			westPanel.revalidate();
		}
	}
	
	public Card getActiveCard(int i) {
		return activeCards.get(i).getCard();
	}
	
	public void addActiveCard(Card c) {
		if (c.type == CardType.WHITE) {
			activeCards.add(new CardPanel(c));
		} else {
			System.err.println("Wrong type of card!");
		}
	}
	
	public void updateActiveCards() {
		for (CardPanel c : activeCards)
			centerDisplay.add(c);
		centerDisplay.revalidate();
	}
	
	public void clearActiveCards() {
		activeCards.clear();
		centerDisplay.removeAll();
		centerDisplay.revalidate();
	}

	public ArrayList<Card> getSelectedCards() {
		ArrayList<Card> result = new ArrayList<Card>();
		for (int i = 0; i < activeCards.size(); i++)
			if (activeCards.get(i).isSelected())
				result.add(activeCards.get(i).getCard());
		return result;
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == countdownTimer) { // timer event
			if (timeRemaining > 0.0) {
				timeLabel.setText(String.format("Time Remaining: %.2f!", timeRemaining));
				timeRemaining -= 0.04;
			} else {
				// time's up.
				stopTimer();
				ArrayList<Card> temp = new ArrayList<Card>();
				temp.add(activeCards.get(0).getCard());
				parent.submitCards(temp);
				JOptionPane.showMessageDialog(this,
				    "Time is up. A card had been selected for you.",
				    "Time up!",
				    JOptionPane.WARNING_MESSAGE);
			}
			repaint();
		} else if (e.getActionCommand().equals(PLAY_CARD)) {
			System.out.println(PLAY_CARD);
			if (!parent.submitCards(getSelectedCards())) {
				JOptionPane.showMessageDialog(this,
				    "If you are a normal player, please double-check the number of cards you are trying to play.\n"+
				    "If you are a main player, please wait until all players have submitted their responses "+
				    "before submitting the winning card.",
				    "Failed to submit response!",
				    JOptionPane.ERROR_MESSAGE);
			} else {
				stopTimer();
			}
		}
	}
}