package netgame.cardsagainsthumanity;

import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;

public class Client extends netgame.common.Client {

	private static final int PORT = 7654;

	private ClientGUI clientgui;
	private GameState currentState;
	private Player player;
	private boolean updated;

	public Client(String hubHostName) throws IOException {
		super(hubHostName, PORT);
		currentState = null;
		player = null;
		updated = false;
		setAutoreset(true);

		// must be last;
		// used as a flag for synchronizing the 
		// constructor.
		//clientgui = new ClientGUI(this);
	}
	
	public boolean submitCards(ArrayList<Card> sub) {
		if (sub.size() != 1) // for now...
			return false;
		System.out.println("Submitting card "+sub.get(0).id);
		System.out.println("From player "+player);
		if (currentState.getMainPlayer().getID() == getID()) {
			if (currentState.numResponses() == currentState.numPlayers()-1)
				send(sub.get(0));
			else
				return false;
		} else {
			send(new ResponseMessage(player, sub.get(0)));
		}
		clientgui.disablePlayCards();
		clientgui.setInstructions("Now wait for all the other players to play their cards.");
		return true;
	}

	protected void messageReceived(Object message) {
		System.out.println("Message recieved.");
		if (clientgui == null)
			clientgui = new ClientGUI(this);
		if (message instanceof GameState) { // received a game state
			System.out.println("Recieved game state.");
			currentState = (GameState)message;
			player = currentState.getPlayerFromID(getID());
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					clientgui.applyGameState(currentState, getID());
				}
			});
		} else if (message instanceof ResetMessage) { // received a reset message 
			System.out.println("Reset!");
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					clientgui.clearActiveCards();
					clientgui.enablePlayCards();
					//clientgui.revalidate();
					clientgui.repaint();
					clientgui.startTimer(15f);
				}
			});
		} else {
			System.out.println("Non-standard message received!");
			System.out.println(message);
		}
	}

	public static void main(String[] args) {
		System.out.println("Client startup...");
		try {
			UIManager.setLookAndFeel("com.seaglasslookandfeel.SeaGlassLookAndFeel");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
				    e.toString(),
				    "Problem with the look and feel!",
				    JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
		}
		String serverAddress = JOptionPane.showInputDialog("Enter server address...");
		if (serverAddress == null) {
			System.out.println("User canceled. Exiting...");
			System.exit(0);
		}
		Client user = null;
		try {
			user = new Client(serverAddress);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
				    "Error: "+e.toString()+"\n\nPlease restart the client and try again.",
				    "Error starting the client...",
				    JOptionPane.ERROR_MESSAGE);
			System.exit(-1);
		}
		System.out.println("Exiting main()... She's all yours.");
	}
}
