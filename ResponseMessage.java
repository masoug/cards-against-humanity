package netgame.cardsagainsthumanity;

import java.io.Serializable;

public class ResponseMessage implements Serializable {
	public Player player;
	public Card cardPlayed;
	
	public ResponseMessage(Player p, Card c) {
		this.player = p;
		this.cardPlayed = c;
	}
}