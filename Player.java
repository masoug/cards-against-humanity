package netgame.cardsagainsthumanity;

import java.io.Serializable;
import java.util.ArrayList;

public class Player implements Serializable {
	
	private int id;
	private int score;
	private Card[] whiteCards;
	
	public Player(int id) {
		this.id = id;
		this.score = 0;
		this.whiteCards = new Card[10];
	}
	
	public int getScore() {
		return this.score;
	}
	
	public void incrementScore() {
		this.score++;
	}

	public int getID() {
		return id;
	}
	
	public boolean sameWhiteCards(Player other) {
		for (int i = 0; i < 10; i++)
			if (!whiteCards[i].equals(other.getWhiteCard(i)))
				return false;
		return true;
	}

	public void setWhiteCard(int i, Card c) {
		if (c.type == CardType.WHITE && i < 10)
			whiteCards[i] = c;
		else
			System.err.println("Wrong card type or index value!");
	}
	
	public Card getWhiteCard(int i) {
		if (i < 10)
			return whiteCards[i];
		else {
			System.err.println("Invalid index!");
			return null;
		}
	}
}
