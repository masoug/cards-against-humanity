package netgame.cardsagainsthumanity;

import java.io.Serializable;
import java.util.ArrayList;

public class Submission implements Serializable {
	public ArrayList<Card> cardsPlayed;
	
	public Submission() {
		this.cardsPlayed = new ArrayList<Card>();
	}
}
