package netgame.cardsagainsthumanity;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class CardPanel extends JPanel implements MouseListener {
	
	private Card card;
	private BufferedImage image = null;
	private boolean isSelected = false;
	
	public CardPanel(Card card) {
		this.card = card;
		setPreferredSize(new Dimension(200, 200));
		addMouseListener(this);
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if (image == null) {
			String fileName = "Cards/";//System.getProperty("user.dir")+
			switch (card.type) {
				case WHITE: fileName += "White/"; break;
				case BLK_REG: fileName += "Black/Regular/"; break;
				case BLK_D2P3: fileName += "Black/Draw2Pick3/"; break;
				case BLK_P2: fileName += "Black/Pick2/"; break;
				default: break;
			}
			fileName += "Card-"+card.id+".jpg";
			//System.out.println("Trying to load "+fileName);
			try {
				//image = ImageIO.read(new File(fileName));
				image = ImageIO.read(getClass().getClassLoader().getResource(fileName));
			} catch (Exception e) {
				System.err.println("Failed to load image!");
				System.err.println(e.getMessage());

				g.setColor(Color.RED);
				((Graphics2D)g).setStroke(new BasicStroke(10));
				g.drawLine(0, 0, 198, 198);
				g.drawLine(198, 0, 0, 198);
			}
		}
		
		g.drawImage(image, 0, 0, this);
		
		if (isSelected) { 
			g.setColor(Color.GREEN);
			((Graphics2D)g).setStroke(new BasicStroke(10));
			g.drawLine(0, 0, 198, 0);
			g.drawLine(0, 0, 0, 198);
			g.drawLine(0, 198, 198, 198);
			g.drawLine(198, 0, 198, 198);
			g.setColor(Color.GRAY);
		}
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card c) {
		this.card = c;
		this.image = null;
		repaint();
	}
	
	public boolean isSelected() {
		return isSelected;
	}
	
	public void mouseClicked(MouseEvent arg0) {
		if (card.type == CardType.WHITE) {
			isSelected = !isSelected;
			repaint();
		}
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent arg0) {
	}

	public void mouseReleased(MouseEvent arg0) {
	}
}
