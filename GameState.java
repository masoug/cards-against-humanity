package netgame.cardsagainsthumanity;

import java.io.Serializable;
import java.util.*;

public class GameState implements Serializable {

	private boolean isStarted;
	private Card blackCard;
	private Player mainPlayer;
	private ArrayList<Player> currentPlayers;
	private ArrayList<Integer> tempDeck;
	private TreeMap<Card, Player> responses;
	//private int remainingTime;
	private static final int WHITE_CARD_COUNT = 304;
	private static final int BLACK_CARD_COUNT = 65;
	
	public GameState() {
		this.currentPlayers = new ArrayList<Player>();
		this.tempDeck = new ArrayList<Integer>();
		for (int i = 0; i < WHITE_CARD_COUNT; i++)
			tempDeck.add(i);
		this.responses = new TreeMap();
		this.isStarted = false;
		blackCard = new Card(CardType.BLK_REG, (int)(Math.random()*BLACK_CARD_COUNT));
	}
	
	public void start() {
		mainPlayer = currentPlayers.get((int)(Math.random()*currentPlayers.size()));
		isStarted = true;
	}
	
	public void reset() {
		blackCard = new Card(CardType.BLK_REG, (int)(Math.random()*BLACK_CARD_COUNT));
		responses.clear();
		isStarted = false;
	}
	
	public Player getPlayerFromID(int id) {
		for (int i = 0; i < currentPlayers.size(); i++)
			if (currentPlayers.get(i).getID() == id)
				return currentPlayers.get(i);
		return null;
	}
	
	public Card getBlackCard() {
		return blackCard;
	}

	public void reDealCards() {
		tempDeck.clear();
		for (int i = 0; i < WHITE_CARD_COUNT; i++)
			tempDeck.add(i);
		for (int i = 0; i < currentPlayers.size(); i++)
			for (int j = 0; j < 10; j++)
				currentPlayers.get(i).setWhiteCard(j, deal());
	}

	public void setMainPlayer(Player p) {
		this.mainPlayer = p;
	}

	public boolean isStarted() {
		return this.isStarted;
	}

	public Player getMainPlayer() {
		return this.mainPlayer;
	}

	public void newPlayer(Player p) {
		for (int i = 0; i < 10; i++)
			p.setWhiteCard(i, deal());
		currentPlayers.add(p);
	}

	public void addResponse(Player p, Card c) {
		responses.put(c, p);
	}

	public int numResponses() {
		return responses.size();
	}

	public Card getLastResponse() {
		return (Card)responses.firstKey();
	}

	public Player getPlayerFromCard(Card c) {
		return (Player)responses.get(c);
	}

	public void playerLeft(int playerID) {
		for (int i = 0; i < currentPlayers.size(); i++) {
			if (currentPlayers.get(i).getID() == playerID) {
				for (int j = 0; i < 10; j++)
					tempDeck.add(currentPlayers.get(i).getWhiteCard(j).id);
				currentPlayers.remove(i);
				return;
			}
		}
	}

	public int numPlayers() {
		return currentPlayers.size();
	}
	
	private Card deal() {
		int target = (int)(Math.random()*tempDeck.size());
		Card result = new Card(CardType.WHITE, tempDeck.get(target));
		tempDeck.remove(target);
		return result;
	}
}
