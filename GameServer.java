package netgame.cardsagainsthumanity;

import java.io.IOException;

public class GameServer extends netgame.common.Hub {
	
	private static final int PORT = 7654;
	private GameState state = null;
	
	public GameServer(int port) throws IOException {
		super(port);
		state = new GameState();
		state.reset();
		setAutoreset(true);
	}

	protected void playerConnected(int pID) {
		System.out.println("Player connected! ("+pID+")");
		if (state.numPlayers() > 30) {
			System.out.println("Too many players!");
			return;
		}
		state.newPlayer(new Player(pID));
		if (!state.isStarted() && state.numPlayers() > 1) {
			state.start();
			sendToAll(new ResetMessage());
		}
		sendToAll(state);
	}

	protected void messageReceived(int playerID, Object message) {
		System.out.println("Message recieved: ");
        if (message instanceof ResponseMessage) { // responses...
        	System.out.println("\tResponse message!");
        	ResponseMessage res = (ResponseMessage)message;
        	System.out.println("\t\tPlayer: "+res.player.getID()+" Card Played: "+res.cardPlayed);
        	state.addResponse(res.player, res.cardPlayed);
        	System.out.println("\tRESPONSE RECORDED.");
        	System.out.println(state.numResponses());
        } else if (message instanceof Card) {
        	System.out.println("\tCard message!");
        	if (state.numResponses() == state.numPlayers()-1) {
        		System.out.println("\t\tAll responses are complete!");
        		Card winner = (Card)message;
        		System.out.print(winner);
        		System.out.println(" is the winner! :)");
        		Player winningPlayer = state.getPlayerFromCard(winner);
        		winningPlayer.incrementScore();
        		state.getPlayerFromID(winningPlayer.getID()).incrementScore();
        		state.setMainPlayer(winningPlayer);
        		state.reset();
        		state.reDealCards();
        		sendToAll(new ResetMessage());
        		state.start();
        	} else {
        		System.out.println("All players need to respond!");
        	}
        }
        sendToAll(state);
    }

	protected void playerDisconected(int pID) {
		System.out.println("Player disconnected.");
		state.playerLeft(pID);
		sendToAll(state);
	}

	public static void main(String[] args) {
		System.out.println("Starting game server...");
		GameServer server = null;
		try {
			server = new GameServer(PORT);
		} catch (IOException e) {
			System.err.println("Failed to start server!");
			e.printStackTrace();
		}
		System.out.println("Server listening on port "+PORT+".");
	}

}
