package netgame.cardsagainsthumanity;

import java.io.Serializable;

public class Card implements Serializable, Comparable {
	
	public CardType type;
	public int id;
	
	public Card(CardType type, int id) {
		this.type = type;
		this.id = id;
	}

	public String toString() {
		return "Card id "+id;
	}
	
	public boolean equals(Card c) {
		return (type == c.type) && (id == c.id);
	}

	public int compareTo(Object c) {
		return equals((Card)c) ? 0 : -1;
	}
}